<html>
	<head>
		<title>Manga Mango</title>
		<link rel="stylesheet" href="style.css">
		<link rel="icon" href="pageIcon.png">
		<style> 
			body {
				text-align: center;
			}

			body * {
				text-align: initial;
			}

			body div {
				display: inline-block;
			}
			
			.err{
				border-color: red;
				box-shadow:0 0 8px 0 red;
			}
			.errmsg{
				color: red;
				font-size: 20px;
			}
		</style>
	</head>
	
	<body>
		<?php
			$_GET['wrongpass'] = false;

		?>
		
		<p class="hidden" id=<?php if ($_GET['wrongpass']===true) { echo '"TrueWrong"'; } else { echo '"correctpass"'; }?>></p>
		<div class="wrap" style="padding-top: 5vh;">
			<div class="center" style="display: flex;">
				
				<form method="post" onsubmit="return validateForm();" action="" onkeypress="return event.keyCode !=13;">
					<input type="submit" class="linkbutton" onclick="change_login(); return false" value="Login" style="float: none; border-radius: 10px;">
					<input type="submit" class="linkbutton" onclick="change_register(); return false" value="Register" style="float: none; border-radius: 10px;"><br>
					<input type="text" placeholder="username" name="username" id="usernamebox" style="flex: 1;"><br>
					<input type="text" placeholder="password" name="password" id="passwordbox" style="flex: 1; -webkit-text-security: disc;"><br>
					<input type="text" placeholder="email" name="email" id="emailbox" style="flex: 1;"><br>
					<input type="submit" class="linkbutton" onclick="redirect_to_check();" id="subbutton" name="register" value="Forgotten Password" style="float: none; border-radius: 10px;">
					<input type="submit" class="linkbutton" id="fbutton" name="Submit" value="Submit" style="float: none; border-radius: 10px;" href="formcheck.php"><br>
					<p class="hidden" id="paragrapherino"></p>
					<?php include("the_php.php") ?>	
				</form>
			</div>
		</div>
		
		<script>
		
			function validateemail(string){
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(String(string).toLowerCase());
			}
			
			function validateForm(){	
				
				var a,b,c,d,e,g,l;
				e = document.getElementById("paragrapherino");
				if (!e.classList.contains("hidden")){
					e.classList.add("hidden");
					e.textContent=""
				}
				
				d = 0;
				g = 0;
				
				function get_email_hidden(){
					var z = document.getElementById("emailbox");
					if (z.classList.contains("hidden")){
						return true;
					}
				}
				
				function take_away_red_border(id){
					l = document.getElementById(id);

					if (l.classList.contains("err")){
						l.classList.remove("err");
					}
				}
				
				take_away_red_border("emailbox"); take_away_red_border("passwordbox"); take_away_red_border("usernamebox");
				
				function addRedBorder(id){
					a = document.getElementById(id);
					if (!a.classList.contains("err")){
						a.classList.add("err");
					}
				}
				

				
				function errMsg(msg){
					var f;
					f = document.getElementById("paragrapherino");
					if (!f.textContent==""){
						return false;
					}
					
					if (f.classList.contains("hidden")){
						f.classList.remove("hidden")
						f.classList.add("errmsg");
						f.textContent+=msg;
					}
					else{
						return;
					}
				}
					
				if (document.getElementById("usernamebox").value==null || document.getElementById("usernamebox").value==""){
					console.log("here");
					addRedBorder("usernamebox");
					d++;
				}
				
				if (document.getElementById("passwordbox").value==null || document.getElementById("passwordbox").value==""){
					addRedBorder("passwordbox");
					d++
				}
				
				if ((document.getElementById("emailbox").value==null || document.getElementById("emailbox").value=="") && !document.getElementById("emailbox").classList.contains("hidden")){
					addRedBorder("emailbox");
					d++
				}

				if (d > 0){

					errMsg("You must enter all fields");
					return false;
				}
				
				
				if (document.getElementById("usernamebox").value.length < 8){
					addRedBorder("usernamebox");
					errMsg("Your username must be longer than 8 characters");
					g++
				}
				
				if (document.getElementById("passwordbox").value.length<8){
					addRedBorder("passwordbox");
					errMsg("Your password must be longer than 8 characters");
					g++
				}
				
				if ((!validateemail(document.getElementById("emailbox").value)) && !get_email_hidden()){
					addRedBorder("emailbox");
					errMsg("You must enter a valid email");
					g++
				}
				
				if (g > 0) {
					return false;
				}
				
				if (document.getElementById("TrueWrong")!==null){
					addRedBorder("passwordbox");
					console.log("you have entered the wrong password");
					errMsg("you have entered the wrong password");
					return false;
				}
				
				return true;
			}
				
			function change_register(){
				var y; 
				y = document.getElementById('emailbox');
				if (y.classList.contains("hidden")){
					y.classList.remove("hidden");
				}
				else {
					return;
				}
			}
			
			function change_login(){
				var y; 
				y = document.getElementById("emailbox");
				if (!y.classList.contains("hidden")){
					y.classList.add("hidden");
				}
				else {
					return;
				}
			}
			
			function redirect_to_check(){
				document.location.href = "formcheck.php";
			}

		</script>
			
	</body>
</html>