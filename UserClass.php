<?php
/*Store this information in a new table on the database*/

class User_ {
	public function __construct ( ) {
		
		if (!array_key_exists('username', $_SESSION)){
			/*Basically jsut throws a new error when the user is not logged in, if I catch this specific error when making a new object, it will be caught, and dealt with.  */
			throw new Exception("The user is not logged in");	//this is the easiest way to just stop everything. It may take a *few* more lines of code in the main file, but in time it saves alot of work. 
		}
		
		$this->time_    				   = time();
		$this->con       				   = mysqli_connect("localhost", "root", "", "mangomanga", 3306);
		$this->username					   = $_SESSION['username'];
		//getting information from SQL
		$this->QUERY_HAS_UPLOADED 		   = "SELECT has_uploaded FROM userinfo WHERE username='$this->username';"; //get has_uploaded
		$this->QUERY_LAST_UPLOAD       	   = "SELECT  last_upload FROM userinfo WHERE username='$this->username';"; //get the last upload (last_upload)
		$this->QUERY_UPLAOD_TIMES      	   = "SELECT upload_times FROM userinfo WHERE username='$this->username';"; //get the amount of times the user has uploaded
		$this->QUERY_IS_ADMIN              = "SELECT     is_admin FROM userinfo WHERE username='$this->username';"; //gets whether they are admin
 
		//updating information
		//$this->QUERY_UPDATE_HAS_UPLOADED   = "UPDATE userinfo SET has_uploaded=1   				     WHERE username='$this->username;"; //updates if they have uploaded
		//$this->QUERY_UPDATE_LAST_UPLOAD    = "UPDATE userinfo SET  last_upload=$this->time_          WHERE username='$this->username;"; //updates the last time they uploaded
		//$this->QUERY_UPDATE_UPLOAD_TIMES   = "UPDATE userinfo SET upload_times='$this->upload_times' WHERE username='$this->username;"; //updates the amount of times they uploaded
		
	}
	

	public function MAKE_NEW_USER($the_username_) {
		
		$this->QUERY_MAKE_NEW_USER = "INSERT INTO userinfo(username, has_uploaded, last_upload, upload_times, is_admin) VALUES ('$the_username_', 0, NULL, 0, 0);";
		echo $this->QUERY_MAKE_NEW_USER;
		$retval = mysqli_query($this->con, $this->QUERY_MAKE_NEW_USER);
		
		if (!$retval) {
			return false;
		}
		
		return true;
	}
	
	public function isAllowedToUpload(){
		$retval = mysqli_query($this->con, QUERY_HAS_UPLOADED);
		$this->time__ = time();
		if ($retval===1){
			$retval = mysqli_query($this->con, QUERY_UPDATE_HAS_UPLOADED);
			$retval = mysqli_query($this->con, QUERY_UPDATE_LAST_UPLOAD);
			
			return true;
		}
		
		//from this point onwards, the user will have uploaed before, therefore, greater checks are needed to stop spam. 
		//I will use the time command to measure how many seconds it has  been before they have uploaded
		
		//Although Adrian may tell you otherwise, artificial intelligcence is just a bunch of 'if' statements.
		
		//The 'time()' command returns, in seconds, how many days it was since the last unix epoch. To prevent spam, I 
		//only allow the users to upload every twenty-two hours. To do this, I check when they last uploaded, and then 
		//if the number of seconds on the time command minus the time whern they uploaded is bigger than 72000, which is 
		//22 hours in seconds, I let them upload.
		
		$retval = mysqli_query($this->con, $this->QUERY_LAST_UPLOAD);
		if ((int(time()) - $retval) > 72000){
			return true;
		} else {return false;}
	}
	
	private function checkUserExists(){ 
		$uname = $_SESSION['username'];
		$query = "SELECT passord FROM logininfo WHERE username='$uname'";
		$retval = mysqli_query($this->con, $query);
		
		if (!retval){
			return true;
		return false;
		}
	}
	
	public function isAdmin() {
		
		$retval = mysqli_fetch_array(mysqli_query($this->con, $this->QUERY_IS_ADMIN), MYSQLI_ASSOC)['is_admin'];

		if ($retval){
			return true;
		} else {
			return false;
		}
	}
	
	public function make_user_admin($the_username_) {

		$the_username_ = mysqli_escape_string($this->con, $the_username_);
		
		//checking that the user exists, ifd they don't then the method will return false;
		$this->QUERY_VERIFY_USER_EXISTS    = "SELECT  is_admin FROM userinfo     WHERE username='$the_username_';";
		$retval_verify = mysqli_query($this->con, $this->QUERY_VERIFY_USER_EXISTS);
		
		if (!mysqli_fetch_array($retval_verify, MYSQLI_ASSOC)['is_admin'] || mysqli_fetch_array(mysqli_query($this->con, $this->QUERY_IS_ADMIN), MYSQLI_ASSOC)['is_admin']) {
			return false;
		}
	
		$this->QUERY_MAKE_USER_ADMIN       = "UPDATE userinfo SET     is_admin=1 WHERE username='$the_username_';"; //makes the user an admin, take alot of care when using this. 

		$retval = mysqli_query($this->con, $this->QUERY_MAKE_USER_ADMIN );
		
		
		return true;
	}
}